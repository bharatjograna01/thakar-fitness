import React, { Component } from 'react';

class Programm extends Component {
    render() {
        return (
            <div>

                {/** bodyFit */}
                <section class="ftco-section">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-md-6 pr-md-5">
                                <div class="heading-section text-md-right ftco-animate fadeInUp ftco-animated">
                                    <span class="subheading">Programs</span>
                                    <h2 class="mb-4">Fitness Programs</h2>
                                    <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                                    <p><a href="#" class="btn btn-primary btn-outline-primary px-4 py-3">View Full Programs</a></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="program d-flex ftco-animate fadeInUp ftco-animated">
                                    <div class="icon d-flex justify-content-center align-items-center">
                                        <span class="flaticon-gym"></span>
                                    </div>
                                    <div class="text ml-5">
                                        <h3>Body Building</h3>
                                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                                    </div>
                                </div>
                                <div class="program d-flex ftco-animate fadeInUp ftco-animated">
                                    <div class="icon d-flex justify-content-center align-items-center">
                                        <span class="flaticon-woman"></span>
                                    </div>
                                    <div class="text ml-5">
                                        <h3>Aerobic Classes</h3>
                                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                                    </div>
                                </div>
                                <div class="program d-flex ftco-animate fadeInUp ftco-animated">
                                    <div class="icon d-flex justify-content-center align-items-center">
                                        <span class="flaticon-workout"></span>
                                    </div>
                                    <div class="text ml-5">
                                        <h3>Weight Lifting</h3>
                                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                                    </div>
                                </div>
                                <div class="program d-flex ftco-animate fadeInUp ftco-animated">
                                    <div class="icon d-flex justify-content-center align-items-center">
                                        <span class="flaticon-meditation"></span>
                                    </div>
                                    <div class="text ml-5">
                                        <h3>Yoga Classes</h3>
                                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                                    </div>
                                </div>
                                <div class="program d-flex ftco-animate fadeInUp ftco-animated">
                                    <div class="icon d-flex justify-content-center align-items-center">
                                        <span class="flaticon-stationary-bike"></span>
                                    </div>
                                    <div class="text ml-5">
                                        <h3>Cardio Training</h3>
                                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>



            </div>
        );
    }
}

export default Programm;