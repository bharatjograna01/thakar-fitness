import React, { Component } from 'react';

class Trainers extends Component {
    render() {
        return (
            <div>

                {/** bodyfit */}
                <section class="ftco-section">
                    <div class="container">
                        <div class="row justify-content-center mb-5">
                            <div class="col-md-7 heading-section text-center ftco-animate fadeInUp ftco-animated">
                                <h2 class="mb-4">Our Coaches</h2>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 d-flex">
                                <div class="coach d-sm-flex align-items-stretch">
                                    <div class="img" style={{ backgroundImage: "(images/trainer-1.jpg)" }}></div>
                                    <div class="text py-4 px-5 ftco-animate fadeInUp ftco-animated">
                                        <span class="subheading">Owner / Head Coach</span>
                                        <h3><a href="#">Oscar Brook</a></h3>
                                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                                        <ul class="ftco-social-media d-flex mt-4">
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-twitter"></span></a></li>
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-facebook"></span></a></li>
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-instagram"></span></a></li>
                                        </ul>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 d-flex">
                                <div class="coach d-sm-flex align-items-stretch">
                                    <div class="img" style={{ backgroundImage: "(images/trainer-2.jpg)" }}></div>
                                    <div class="text py-4 px-5 ftco-animate fadeInUp ftco-animated">
                                        <span class="subheading">Owner / Head Coach</span>
                                        <h3><a href="#">Leonard Smith</a></h3>
                                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                                        <ul class="ftco-social-media d-flex mt-4">
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-twitter"></span></a></li>
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-facebook"></span></a></li>
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-instagram"></span></a></li>
                                        </ul>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 d-flex">
                                <div class="coach d-sm-flex align-items-stretch">
                                    <div class="img order-xl-last" style={{ backgroundImage: "(images/trainer-3.jpg)" }}></div>
                                    <div class="text py-4 px-5 ftco-animate fadeInUp ftco-animated">
                                        <span class="subheading">Owner / Head Coach</span>
                                        <h3><a href="#">James Buffer</a></h3>
                                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                                        <ul class="ftco-social-media d-flex mt-4">
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-twitter"></span></a></li>
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-facebook"></span></a></li>
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-instagram"></span></a></li>
                                        </ul>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 d-flex">
                                <div class="coach d-sm-flex align-items-stretch">
                                    <div class="img order-xl-last" style={{ backgroundImage: "(images/trainer-4.jpg)" }}></div>
                                    <div class="text py-4 px-5 ftco-animate fadeInUp ftco-animated">
                                        <span class="subheading">Owner / Head Coach</span>
                                        <h3><a href="#">Maricar Collins</a></h3>
                                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                                        <ul class="ftco-social-media d-flex mt-4">
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-twitter"></span></a></li>
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-facebook"></span></a></li>
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-instagram"></span></a></li>
                                        </ul>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 d-flex">
                                <div class="coach d-sm-flex align-items-stretch">
                                    <div class="img" style={{ backgroundImage: "(images/trainer-5.jpg)" }}></div>
                                    <div class="text py-4 px-5 ftco-animate fadeInUp ftco-animated">
                                        <span class="subheading">Owner / Head Coach</span>
                                        <h3><a href="#">Patrece Miller</a></h3>
                                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                                        <ul class="ftco-social-media d-flex mt-4">
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-twitter"></span></a></li>
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-facebook"></span></a></li>
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-instagram"></span></a></li>
                                        </ul>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 d-flex">
                                <div class="coach d-sm-flex align-items-stretch">
                                    <div class="img" style={{ backgroundImage: "(images/trainer-6.jpg)" }}></div>
                                    <div class="text py-4 px-5 ftco-animate fadeInUp ftco-animated">
                                        <span class="subheading">Owner / Head Coach</span>
                                        <h3><a href="#">Leonard Smith</a></h3>
                                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                                        <ul class="ftco-social-media d-flex mt-4">
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-twitter"></span></a></li>
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-facebook"></span></a></li>
                                            <li class="ftco-animate fadeInUp ftco-animated"><a href="#" class="mr-2 d-flex justify-content-center align-items-center"><span class="icon-instagram"></span></a></li>
                                        </ul>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        );
    }
}

export default Trainers;