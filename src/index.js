/**
 * Developed By: Bharat Jograna
 * Created on: 17 Sep 2019
 * index component: index of this project
*/
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

ReactDOM.render(
    <App />,
    document.getElementById('root')
);